import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.indigo.shade500,
          )
        )
      ),

      home: Scaffold(
        // appBar: AppBar(
        //   backgroundColor: Colors.tealAccent,
        appBar: AppBar(
          backgroundColor: Colors.tealAccent,
          leading: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),

          actions: <Widget>[
            IconButton(onPressed: () {},
                icon: Icon(Icons.star_border),
                color: Colors.black,
            )
          ],
        ),
        body: ListView (
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  //Height constraint at Container widget level
                  height: 250,
                  child: Image.network("https://images5.alphacoders.com/587/587597.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      child: Text("Apassara Wannawijit",
                      style: TextStyle(fontSize: 30),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  thickness: 0.8,
                ),

                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildCallButton(),buildTextButton(),
                      buildVideoCallButton(),
                      buildEmailButton(),
                      buildDirectionsButton(),
                      buildPayButton(),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  thickness: 0.8,
                ),
                mobilePhoneListTile(),
                otherPhoneListTile(),
                Divider(
                  color: Colors.grey,
                  thickness: 0.8,
                ),
                emailListTile(),
                Divider(
                  color: Colors.grey,
                  thickness: 0.8,
                ),
               addressListTile(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

//  ListTile
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton (
        icon: Icon(Icons.message),
      color: Colors.cyanAccent,
      onPressed: () {},
    ),
  );
}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton (
      icon: Icon(Icons.message),
      color: Colors.cyanAccent,
      onPressed: () {},
    ),
  );
}
Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160084@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: Text(""),
  );
}
Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("115/30 province Chanthaburi"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.cyanAccent,
      onPressed: () {},
    ),
  );
}
Widget buildAppbar(){
  return AppBar(
    backgroundColor: Colors.tealAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),

    actions: <Widget>[
      IconButton(onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.black,
      )
    ],
  );
}